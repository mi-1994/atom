module.exports = {
  presets: [
    // 预设一套插件
    [
      '@babel/env', // 支持环境
      {
        targets: {
          chrome: '64',
          safari: '11.1',
          edge: '17',
          firefox: '60',
        },
        useBuiltIns: 'usage', // 使用嵌入插件，自动补充执行环境没有的插件
        corejs: 3, // 使用core3js的自动补充嵌入插件
      },
    ],
    '@babel/typescript', // 预设一组ts插件
    '@babel/preset-react', // 预设一组react插件
  ],
  plugins: [
    '@babel/proposal-class-properties', // 类属性
    '@babel/proposal-object-rest-spread', // 对象展开
    [
      'module-resolver', // 解析指导文件 jsx ts tsx
      {
        root: ['./src'],
        extensions: ['.js', '.jsx', '.ts', '.tsx'],
      },
    ],
    [
      'styled-components',
      {
        ssr: true,
        fileName: true, // 以组件名称命名className
        minify: true, // 压缩
        transpileTemplateLiterals: true, // 去除空格
        pure: true, // 死代码消除
      },
    ],
  ],
};
