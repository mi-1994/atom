FROM node:14-alpine

ENV APOLLO_HOST='test123'

ENV APOLLO_HOST2=$APOLLO_HOST2

EXPOSE 8081
# 定位项目 文件地址
WORKDIR /atom
# 复制文件
COPY . .
# 通过node 执行server的启动文件
CMD ["node", "src/server/index.ts"]