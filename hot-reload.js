const fs = require('fs');
const net = require('net');
const { WebSocketServer } = require('ws');

const websocket = new WebSocketServer({ port: 4000 });

// 检测端口是否被占用
function portIsOpening(port, reload) {
  // 创建服务并监听该端口
  const server = net.createServer().listen(port);
  // 当端口被占用，则代表重启完毕，热重载页面
  server.on('error', () => {
    reload();
  });
  // 关闭服务
  server.close();
}

websocket.on('connection', (ws) => {
  // 监听客户端打包后的文件变化，变化后通知连接该socket的游览器reload
  fs.watch('./build/client', () => {
    ws.send('reloading');
    const timer = setInterval(() => {
      portIsOpening(8081, () => {
        ws.send('reload');
        clearInterval(timer);
      });
    }, 100);
  });
});
