const fs = require('fs');
const { resolve } = require('path');
// eslint-disable-next-line
const { spdy, app, staticServer } = require(resolve('build/server/app.js'));

const options = {
  key: fs.readFileSync(resolve('ssl/6701851_mi-atom.com.key'), 'utf8'),
  cert: fs.readFileSync(resolve('ssl/6701851_mi-atom.com.pem'), 'utf8'),
};

app.use('/app.js', staticServer(resolve('build/client/app.js'))); // 客户端spa渲染模式js，设置为静态文件。在初始html中使用
app.use('/app.js.map', staticServer(resolve('build/client/app.js.map')));
app.use('/static', staticServer(resolve('build/static')));

spdy.createServer(options, app).listen(443);
