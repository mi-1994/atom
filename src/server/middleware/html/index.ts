/**
 *
 * @param {*} renderStr react组件使用renderToString()生成
 * @param {*} dataStr 服务端预取的数据
 * @param {*} styleTags styled-components 样式表标签
 * @returns
 */
export const initHtml = (renderStr, dataStr, styleTags) => `
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="minimum-scale=1, initial-scale=1, maximum-scale=1, width=device-width, user-scalable=no" />
    <title>前端技术栈</title>
    <link href="https://cdn.bootcdn.net/ajax/libs/animate.css/4.1.1/animate.min.css" rel="stylesheet" type="text/css"/>
    ${styleTags}
  </head>
  <body>
    <div id="root">${renderStr}</div>
    <script crossorigin="anonymous" src="https://browser.sentry-cdn.com/7.7.0/bundle.tracing.min.js" integrity="sha384-lr/bDcE0vmUPBU6dhVBXfmhNYAEilrIepu1BRdVUJ8mZ0Hxhm17aIJ2bt4UfxC72"></script>
    <script>Sentry?.onLoad(function() {
      try {
        Sentry.init({
          integrations: [new Sentry.BrowserTracing()],
          dsn: 'https://2d245de029c64ae383fc96b8c99ea24a@o588082.ingest.sentry.io/6349504',
        })
      } catch (error) {
        console.warn(error)
      }
    })</script>
  </body>
  <script type="application/json" id="__SSR_DATA__">${dataStr}</script>
  <script type="text/javascript" src="/app.js" refer></script>
</html>
`;
