// 服务端缓存cache，模拟redis

class Cache {
  redisCache: Record<string, string>;

  constructor() {
    this.redisCache = {};
  }

  get(path) {
    const cache = this.redisCache[path];
    // 命中缓存，使用cache [性能优化]
    return cache;
  }

  set(path, html) {
    this.redisCache[path] = html;
  }

  remove(router) {
    router.get('/cache/remove', (request, response) => {
      const hasCache = Object.keys(this.redisCache).length === 0;
      const text = hasCache ? 'no cache' : 'success';

      this.redisCache = {};
      response.end(text);
    });
  }
}

export default new Cache();
