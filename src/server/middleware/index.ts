import spdy from 'spdy';
import express, { static as staticServer } from 'express';
import router from './router';

const app = express();

app.use(router); // 处理 路由 预取 渲染 缓存 的中间件

export { spdy, app, staticServer };
