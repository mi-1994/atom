import React from 'react';
import { renderToString } from 'react-dom/server';
import { StaticRouter } from 'react-router-dom/server';
import { ServerStyleSheet } from 'styled-components';
import GlobalThemeStyle from 'components/theme';
import { initHtml } from '../html';

export async function renderHtml(Component, query, url) {
  // 组件静态方法，获取数据 [数据同构]
  const props = await Component?.getInitProps?.(query);
  // 将数据注入html，保证双端节点对比成功 [渲染同构]
  const dataStr = JSON.stringify({ props, url });
  // 创建server端样式表
  const sheet = new ServerStyleSheet();
  const app = (
    <GlobalThemeStyle>
      <StaticRouter location={url}>
        <Component {...(props || {})} />
      </StaticRouter>
    </GlobalThemeStyle>
  );
  // 将样式表和组件结合
  const appWithStyle = sheet.collectStyles(app);
  // react组件转换为html字符串
  const renderStr = renderToString(appWithStyle);
  // 获取样式表style标签，以及所有样式
  const styleTags = sheet.getStyleTags();
  // 服务端直出的页面html
  const html = initHtml(renderStr, dataStr, styleTags);
  // 样式表密封
  sheet.seal();
  return html;
}
