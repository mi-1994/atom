import express from 'express';
import routes from 'routes';
import ssrCache from '../cache';
import { renderHtml } from '../render';

const router = express.Router();
// 遍历项目中的路由
routes.forEach(({ path, Component }) => {
  // 服务端路由 [路由同构]
  router.get(path, async ({ params, url }, response) => {
    const cacheHtml = ssrCache.get(url);
    // 命中缓存，直接返回
    if (cacheHtml) {
      response.end(cacheHtml);
      return;
    }
    // 获取已经异步得到数据已经结合样式表的html
    const html = await renderHtml(Component, params, url);
    // 目前所有页面都进行了缓存
    ssrCache.set(url, html);
    response.end(html);
  });
});
router.get('/env', (req, response) => {
  response.end(`${process.env.APOLLO_HOST},${process.env.ZWL},${process.env.APOLLO_HOST2},1234`);
});

ssrCache.remove(router);

export default router;
