import React from 'react';
import axios from 'axios';
import { Link } from 'react-router-dom';
import bgPng from './img/bg.png';
import { StyledDemo } from './style';

const api = 'https://gw.datayes-stg.com/rrp_mammon_stg/mobile/whitelist/classroom/secretary';

const get = (url) => axios.get(url).then(({ data }) => data.data);

function click() {
  alert('react ssr的交互');
}

const Demo = ({ list }: any) => (
  <StyledDemo>
    <h1>
      <span>首页自动构建部署11</span>
    </h1>
    <div
      onClick={() => {
        throw new Error('atom sentry error4');
      }}
    >
      sentry 报错4
    </div>
    <img src={bgPng} alt="" />
    <div onClick={click}>点我进行交互</div>
    <h2>
      <Link to="/login">跳转到登录页</Link>
    </h2>
    <h2>下面是服务端请求接口 预取到的数据：</h2>
    {list?.map(({ title, content }) => (
      <div key={title}>
        <div>{title}</div>
        <div>{content}</div>
      </div>
    ))}
  </StyledDemo>
);

Demo.getInitProps = async () => {
  const list = await get(api);

  return { list };
};

export default Demo;
