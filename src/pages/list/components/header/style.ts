import styled from 'styled-components';

export const StyleHeader = styled.div`
  position: relative;
  height: 52vw;
  margin-bottom: 20px;
  box-shadow: 0px 12px 18px rgb(232 86 86 / 60%);
  > img {
    position: absolute;
    &.bg {
      width: 100vw;
    }
    &.gift {
      width: 30vw;
      right: 20px;
      bottom: -36px;
    }
    &.cloud-right {
      width: 8vw;
      bottom: 20px;
      right: 30vw;
    }
    &.cloud-left-top {
      width: 8vw;
      right: 55vw;
      top: 20vw;
    }
    &.cloud-left-bottom {
      width: 12vw;
      left: 50px;
      bottom: -15px;
    }
  }
  > div {
    position: absolute;
    color: #fff;
    &.text {
      left: 20px;
      top: 20vw;
      font-size: 15px;
    }
    &.title {
      left: 20px;
      top: 28vw;
      font-weight: bold;
      font-size: 25px;
    }
  }
`;
