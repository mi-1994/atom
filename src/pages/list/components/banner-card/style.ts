import styled from 'styled-components';
import { flexColumnCss, flexCenterCss, lineEllipsisCss } from 'styles/common';

export const StyledHeaderCard = styled.div`
  display: flex;
  justify-content: space-between;
  height: 160px;
  padding: 20px;
  margin: 20px;
  border-radius: 10px;
  background-image: linear-gradient(to bottom right, rgba(255, 97, 97, 1), rgba(227, 66, 66, 1));
  box-shadow: 9px 7px 18px rgba(232, 86, 86, 0.6);
  > div {
    :nth-child(1) {
      width: calc(100vw - 180px);
      color: ${(props) => props.theme.colorFont3};
      ${flexColumnCss};
      > div {
        line-height: 20px;
        color: rgba(255, 255, 255, 0.8);
        ${lineEllipsisCss(2)};
      }
    }
    :nth-child(2) {
      height: 80px;
      width: 80px;
      border-radius: 4px;
      background-color: ${(props) => props.theme.colorFont3};
      img {
        width: 60px;
      }
    }
  }
`;

export const StyledLineBtn = styled.p`
  width: max-content;
  padding: 8px;
  border-radius: 4px;
  border: 1px solid rgba(255, 255, 255, 1);
  ${flexCenterCss};
`;
