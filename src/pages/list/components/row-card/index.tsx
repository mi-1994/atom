import React from 'react';
import { TagText, TagBtn } from 'components/ui/tag';
import { Image } from 'components/ui/image';
import { Props } from '../../type';
import { StyledRowCard, StyledP, StyledFlexColumn, StyledFlexRow, StyledLeftTop } from './style';

export const RowCard = ({ list }: Props) => (
  <div>
    {list.map(({ name, tag, content, time, src, isOpen }) => (
      <StyledRowCard key={name} isOpen={isOpen}>
        <StyledLeftTop>hot</StyledLeftTop>
        <Image width={90} height={90} right={10} src={src} />
        <StyledFlexColumn>
          <div>
            <h3>
              <TagText text={tag} />
              {name}
            </h3>
            <StyledP>{content}</StyledP>
          </div>
          <StyledFlexRow>
            <TagBtn text="热点" />
            <time>{time}</time>
          </StyledFlexRow>
        </StyledFlexColumn>
      </StyledRowCard>
    ))}
  </div>
);
