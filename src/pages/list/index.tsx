import React from 'react';
import { RowBorderTitle } from 'components/ui/row';
import Header from './components/header';
import { RowCard } from './components/row-card';
import { getList } from './service';
import { Props } from './type';
import { StyleList } from './style';

function List({ list }: Props) {
  return (
    <StyleList>
      <Header />
      <RowBorderTitle />
      <RowCard list={list} />
    </StyleList>
  );
}

List.getInitProps = async () => {
  const list = await getList();

  return { list };
};

export default List;
