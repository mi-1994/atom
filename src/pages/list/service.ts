import { get } from 'utils/request';

export const getList = () => get('article/list').catch(() => []);
