import React from 'react';
import ReactDom from 'react-dom';
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import GlobalThemeStyle from 'components/theme';
import routes from '../routes';

const initProps = ({ props, url }, path): any => (url.includes(path) ? props : {});

function clientRender() {
  // 从页面中获取服务端注入的预取的数据，并通过props传入组件
  const ssrData = JSON.parse(document.getElementById('__SSR_DATA__')?.innerText) || {};

  const app = (
    <GlobalThemeStyle>
      <BrowserRouter>
        <Routes>
          {routes.map(({ path, page, Component }) => (
            <Route
              key={path}
              path={path}
              element={<Component {...initProps(ssrData, page || path)} />}
            />
          ))}
        </Routes>
      </BrowserRouter>
    </GlobalThemeStyle>
  );

  ReactDom.hydrate(app, document.getElementById('root'));
}

// 客户端渲染入口
clientRender();
