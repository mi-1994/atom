import React from 'react';
import { useRequest } from 'ahooks';
import { useNav } from 'hooks/path';
import { Image } from 'components/ui/image';
import { getList } from './service';
import { StyleColumn, StyleCard } from './style';

function Column({ list }: any) {
  const push = useNav();
  const { data } = useRequest(getList, { initialData: list });

  return (
    <StyleColumn>
      <h2>
        <span>Hi 刘全有</span>
        <Image width={40} radius={20} />
      </h2>
      <h1>欢迎来到专栏广场</h1>
      <main>
        {data?.map(({ name, desc, _id }) => (
          <StyleCard key={_id} onClick={() => push(`/column/detail/${_id}`)}>
            <Image width={48} radius={24} />
            <h2>{name}</h2>
            <p>{desc}</p>
          </StyleCard>
        ))}
      </main>
    </StyleColumn>
  );
}

Column.getInitProps = async () => {
  const list = await getList();

  return { list };
};

export default Column;
