import { get } from 'utils/request';
import { Column } from 'types/column';

export const getList = (): Promise<Column> => get('column/list').catch(() => []);
