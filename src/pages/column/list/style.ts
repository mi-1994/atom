import styled from 'styled-components';
import { lineEllipsisCss, ellipsisCss, cardCss, flexBetCss } from 'styles/common';

export const StyleColumn = styled.div`
  padding: 20px;
  > h2 {
    ${flexBetCss};
  }
  > main {
  }
`;

export const StyleCard = styled.div`
  width: calc((100vw - 60px) / 2);
  display: inline-block;
  ${cardCss};
  > h2 {
    ${ellipsisCss};
  }
  > p {
    height: 36px;
    line-height: 18px;
    margin-top: 10px;
    font-size: 13px;
    color: rgba(37, 40, 50, 0.6);
    ${lineEllipsisCss(2)};
  }
  :nth-child(odd) {
    margin-right: 20px;
  }
`;
