/** Copyright © 2013-2021 DataYes, All Rights Reserved. */

import React from 'react';
import { useAnimate } from 'hooks/animate';
import bgPng from './img/bg.png';
import giftboxPng from './img/giftbox.png';
import cloud1Png from './img/cloud1.png';
import cloud2Png from './img/cloud2.png';
import cloud3Png from './img/cloud3.png';
import { StyleHeader } from './style';

export default function Header() {
  const className = useAnimate();

  return (
    <StyleHeader>
      <img className="bg" src={bgPng} alt="" />
      <img
        className="gift animate__animated animate__bounce animate__infinite"
        src={giftboxPng}
        alt=""
      />
      <img
        className={`cloud-right ${className || 'animate__animated animate__backInLeft'}`}
        src={cloud1Png}
        alt=""
      />
      <div className="text">给自己的礼物！</div>
      <div className="title">2022 新的起点</div>
      <img
        className={`cloud-left-top ${
          className || 'animate__animated animate__backInRight animate__slow'
        }`}
        src={cloud2Png}
        alt=""
      />
      <img
        className={`cloud-left-bottom ${
          className || 'animate__animated animate__fadeInRight animate__slow'
        }`}
        src={cloud3Png}
        alt=""
      />
    </StyleHeader>
  );
}
