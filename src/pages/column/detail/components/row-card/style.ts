import styled from 'styled-components';
import { ellipsisCss, flexCenterCss, flexColumnCss, cardCss } from 'styles/common';

export const StyledRowCard = styled.div<{ isOpen: boolean }>`
  display: flex;
  margin: 20px;
  padding: 10px;
  ${({ isOpen }) => !isOpen && 'filter: blur(2px);'};
  ${cardCss};
`;

export const StyledLeftTop = styled.div`
  position: absolute;
  left: 0;
  top: 0;
  height: 34px;
  width: 34px;
  border-radius: 6px;
  background-color: ${(props) => props.theme.colorFont3};
  ${flexCenterCss};
`;

export const StyledP = styled.p`
  height: 24px;
  width: 100%;
  line-height: 24px;
  font-size: 14px;
  color: ${(props) => props.theme.colorFont2};
  ${ellipsisCss};
`;

// 功能型样式
export const StyledFlexColumn = styled.div`
  width: calc(100vw - 170px);
  padding-bottom: 4px;
  ${flexColumnCss};
`;

export const StyledFlexRow = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
`;

export const StyledFlex = styled.div`
  display: flex;
  flex-direction: column;
  align-items: baseline;
  justify-content: center;
  height: 80px;
  ${ellipsisCss};
`;
