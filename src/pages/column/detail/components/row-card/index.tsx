import React from 'react';
import { TagText, TagBtn } from 'components/ui/tag';
import { Image } from 'components/ui/image';
import { useNav } from 'hooks/path';
import { Props } from '../../type';
import { StyledRowCard, StyledP, StyledFlexColumn, StyledFlexRow, StyledLeftTop } from './style';

export const RowCard = ({ list }: Props) => {
  const navTo = useNav();

  return (
    <>
      {list.map(({ _id, name, tag, content, time, isOpen }) => (
        <StyledRowCard key={_id} isOpen={isOpen} onClick={() => navTo(`/article/detail/${_id}`)}>
          <StyledLeftTop>hot</StyledLeftTop>
          <Image width={90} height={90} right={10} />
          <StyledFlexColumn>
            <div>
              <h3>
                <TagText text={tag} />
                {name}
              </h3>
              <StyledP>{content}</StyledP>
            </div>
            <StyledFlexRow>
              <TagBtn text="热点" />
              <time>{time}</time>
            </StyledFlexRow>
          </StyledFlexColumn>
        </StyledRowCard>
      ))}
    </>
  );
};
