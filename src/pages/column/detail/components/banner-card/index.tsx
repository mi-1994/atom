import React from 'react';
import phonePng from './img/phone.png';
import { StyledHeaderCard, StyledLineBtn } from './style';

export default function BannerCard() {
  return (
    <StyledHeaderCard>
      <div>
        <h3>更精准的技术解析</h3>
        <div>2021新年计划就是 牛年大吉，天天开心。好好学习准备习准备</div>
        <StyledLineBtn>升级专业版服务</StyledLineBtn>
      </div>
      <div>
        <img src={phonePng} alt="" />
      </div>
    </StyledHeaderCard>
  );
}
