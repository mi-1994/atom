import { get } from 'utils/request';

export const getList = (id: string) => get(`article/list?columnId=${id}`).catch(() => []);
