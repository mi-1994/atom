import React from 'react';
import { useParams } from 'react-router-dom';
import { useRequest } from 'ahooks';
import { RowBorderTitle } from 'components/ui/row';
// import Header from './components/header';
import { RowCard } from './components/row-card';
import { getList } from './service';
import { Props } from './type';
import { StyleColumn } from './style';

function Column({ list }: Props) {
  const { id } = useParams();
  const { data } = useRequest(getList, { initialData: list, defaultParams: [id] });

  return (
    <StyleColumn>
      {/* <Header /> */}
      <RowBorderTitle />
      <RowCard list={data || []} />
    </StyleColumn>
  );
}

Column.getInitProps = async ({ id }) => {
  const list = await getList(id);

  return { list };
};

export default Column;
