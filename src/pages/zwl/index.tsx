import React from 'react';
import bgPng from './img/bg.png';
import Time from './time';
import { StyledDemo } from './style';

const Demo = () => (
  <StyledDemo>
    <Time />
    <img src={bgPng} alt="" />
    <div className="day">3</div>
  </StyledDemo>
);

Demo.getInitProps = async () => ({});

export default Demo;
