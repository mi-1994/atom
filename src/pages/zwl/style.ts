import styled from 'styled-components';

export const StyledDemo = styled.div`
  width: 100vw;
  height: 100vh;
  position: relative;
  img {
    width: 100vw;
    height: 100vh;
  }
  .time {
    position: absolute;
    left: 15vw;
    top: 312px;
    width: 70vw;
    text-align: center;
    height: 30px;
    font-size: 15px;
    color: #666;
    letter-spacing: 1px;
    background-color: white;
    span {
      font-size: 18px;
      font-weight: bold;
      color: #333;
    }
  }
  .day {
    position: absolute;
    bottom: 161px;
    font-size: 40px;
    font-weight: bold;
    width: 25px;
    height: 41px;
    left: 80px;
    background-color: white;
  }
`;
