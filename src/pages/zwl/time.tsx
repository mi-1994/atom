/* eslint-disable */

import React, { useEffect, useState } from 'react';

export default function Time() {
  const [time, setTime] = useState('');
  const [second, setSecond] = useState('');

  useEffect(() => {
    (Date as any).prototype.Format = function (formatStr) {
      let fmt = formatStr;
      // author: meizz
      const o = {
        'M+': this.getMonth() + 1, // 月份
        'd+': this.getDate(), // 日
        'h+': this.getHours(), // 小时
        'm+': this.getMinutes(), // 分
        's+': this.getSeconds(), // 秒
        'q+': Math.floor((this.getMonth() + 3) / 3), // 季度
        S: this.getMilliseconds(), // 毫秒
      };
      if (/(y+)/.test(fmt)) {
        fmt = fmt.replace(RegExp.$1, `${this.getFullYear()}`.substr(4 - RegExp.$1.length));
      }
      for (const k in o) {
        if (new RegExp(`(${k})`).test(fmt)) {
          fmt = fmt.replace(
            RegExp.$1,
            RegExp.$1.length == 1 ? o[k] : `00${o[k]}`.substr(`${o[k]}`.length)
          );
        }
      }
      return fmt;
    };
    setInterval(() => {
      const t = (new Date() as any).Format('yyyy-MM-dd hh:mm');
      const s = (new Date() as any).Format('ss');

      setTime(t);
      setSecond(s);
    }, 1000);
  }, []);

  return (
    <div className="time">
      {`${time}:`}
      <span>{second}</span>
    </div>
  );
}
