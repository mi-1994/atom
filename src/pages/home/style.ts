/** Copyright © 2013-2021 DataYes, All Rights Reserved. */

import styled from 'styled-components';

const mainWidth = 65;

export const StyledHome = styled.div`
  position: relative;
  width: 100vw;
  height: 100vh;
  background-color: #22252e;
  > header {
    position: fixed;
    top: 0;
    > img {
      width: 200vw;
      margin-left: -80vw;
      margin-top: -50vw;
    }
  }
  > main {
    width: 100vw;
    top: 25vh;
    position: fixed;
    display: flex;
    justify-content: center;
    > .main {
      position: relative;
      width: ${mainWidth}vw;
      img {
        position: absolute;
        width: ${mainWidth}vw;
        z-index: 3;
      }
      .main2 {
        width: ${mainWidth * 1.09}vw;
        top: 40px;
        z-index: 2;
      }
      .main3 {
        width: ${mainWidth * 1.27}vw;
        top: 60px;
        left: 8px;
        z-index: 1;
      }
      .line1 {
        top: 60px;
      }
      .line2 {
        width: ${mainWidth * 0.5}vw;
        right: calc(${mainWidth}vw - 30px);
      }
      .line3 {
        top: 60px;
      }
    }
  }
  > footer {
    position: fixed;
    width: 100vw;
    bottom: 20vh;
    padding: 0 30px;
    letter-spacing: 1px;
    > a > div {
      position: relative;
      font-size: 26px;
      font-weight: bold;
      color: #fff;
      margin-bottom: 10px;
      :before {
        height: 3px;
        width: 60px;
        top: -10px;
        position: absolute;
        content: ' ';
        background-color: #ff6a6f;
      }
    }
    > p {
      font-size: 12px;
      line-height: 20px;
      color: rgb(166, 166, 166);
    }
    > img {
      position: fixed;
      bottom: 10vh;
      width: 200vw;
      left: -45vw;
      bottom: -55vw;
    }
  }
`;
