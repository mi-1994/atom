/** Copyright © 2013-2021 DataYes, All Rights Reserved. */

import React from 'react';
import { Link } from 'components/ui/button';
// import { useAnimate } from 'hooks/animate';
import main1 from './img/main1.png';
import main2 from './img/main2.png';
import main3 from './img/main3.png';
// import line1 from './img/line1.png';
// import line2 from './img/line2.png';
// import line3 from './img/line3.png';
import bg1 from './img/bg1.png';
import { StyledHome } from './style';

export default function Home() {
  // const className = useAnimate();
  const animateClass = 'animate__animated animate__backInRight animate__slow';

  return (
    <StyledHome>
      <header>
        <img src={bg1} alt="" />
      </header>
      <main className={animateClass}>
        <div className="main">
          <img className="main1" src={main1} alt="" />
          <img className="main2" src={main2} alt="" />
          <img className="main3" src={main3} alt="" />
          {/* <img className="line1" src={line1} alt="" />
          <img className="line2" src={line2} alt="" />
          <img className="line3" src={line3} alt="" /> */}
        </div>
      </main>
      <footer>
        <img src={bg1} alt="" />
        <Link to="/column/list">
          <div>开启前端技术栈</div>
        </Link>
        <p>
          欢迎使用前端技术栈欢迎使用前端技术栈欢迎使用前端技术栈欢迎使用前端技术栈欢迎使用前端技术栈
        </p>
      </footer>
    </StyledHome>
  );
}
