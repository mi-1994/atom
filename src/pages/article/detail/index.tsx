import React from 'react';
import { useParams } from 'react-router-dom';
import { useRequest } from 'ahooks';
import { Html } from 'components/html';
import { StyleDetail } from './style';
import { getInfo } from './service';

function Article({ info }: any) {
  const { id } = useParams();
  const { data } = useRequest(getInfo, { defaultParams: [id], initialData: info });
  const { detail } = data || {};

  return (
    <StyleDetail>
      <Html html={detail} />
    </StyleDetail>
  );
}

Article.getInitProps = async ({ id }) => {
  const info = await getInfo(id);

  return { info };
};

export default Article;
