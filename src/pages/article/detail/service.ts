import { get } from 'utils/request';

export const getInfo = (id: string) => get(`/article/detail/${id}`).catch(() => ({}));
