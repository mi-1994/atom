/** Copyright © 2013-2021 DataYes, All Rights Reserved. */

import styled, { css } from 'styled-components';
import { ellipsisCss, flexCenterCss, lineEllipsisCss } from 'styles/common';

export const theme = {
  colorBg1: 'rgba(240, 242, 245, 1)', // 一级背景色(浅灰色)
  colorBg2: 'rgba(255, 255, 255, 1)', // 二级背景色(白色)
  colorTheme: 'rgba(224, 88, 88, 1)', // 主题色(棕红色)
  colorFont1: 'rgba(3, 3, 3, 1)', // 字号色(黑色)
  colorFont2: 'rgba(188, 188, 188, 1)', // 字号色(灰色)
  colorFont3: 'rgba(255, 255, 255, 1)', // 字号色(白色)
  colorShadow1: 'rgba(0, 0, 0, 0.06)', // 阴影色(透明黑)
};

interface Theme {
  theme: Record<string, string>;
}

const cardCss = css`
  position: relative;
  margin: 10px;
  padding: 10px;
  background-color: ${(props) => props.theme.colorBg2};
  box-shadow: 0px 2px 6px ${(props) => props.theme.colorShadow1};
  border-radius: 4px;
`;

const flexColumnCss = css`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
`;

export const StyledDemo = styled.div<Theme>`
  width: 100vw;
  background-color: ${(props) => props.theme.colorBg1};
`;

export const StyledHeaderCard = styled.div`
  display: flex;
  justify-content: space-between;
  height: 160px;
  padding: 20px;
  margin: 20px;
  border-radius: 10px;
  background-image: linear-gradient(to bottom right, rgba(255, 97, 97, 1), rgba(227, 66, 66, 1));
  box-shadow: 9px 7px 18px rgba(232, 86, 86, 0.6);
  > div {
    :nth-child(1) {
      width: calc(100vw - 180px);
      color: ${(props) => props.theme.colorFont3};
      ${flexColumnCss};
      > div {
        line-height: 20px;
        color: rgba(255, 255, 255, 0.8);
        ${lineEllipsisCss(2)};
      }
    }
    :nth-child(2) {
      height: 80px;
      width: 80px;
      border-radius: 4px;
      background-color: ${(props) => props.theme.colorFont3};
      img {
        width: 60px;
      }
    }
  }
`;

// 业务型样式
export const StyledRowCard = styled.div`
  display: flex;
  margin: 20px;
  padding: 10px;
  height: 110px;
  ${cardCss};
`;

export const StyledColumnCard = styled.div`
  width: 180px;
  height: 250px;
  ${cardCss}
`;

export const StyledColumnTwoCard = styled.div`
  flex-direction: row;
  width: calc((100vw - 60px) / 2);
  height: 180px;
  ${cardCss}
`;

export const StyledLeftTop = styled.div`
  position: absolute;
  left: 0;
  top: 0;
  height: 34px;
  width: 34px;
  border-radius: 6px;
  background-color: ${(props) => props.theme.colorFont3};
  ${flexCenterCss};
`;

export const StyledLineBtn = styled.p`
  width: max-content;
  padding: 8px;
  border-radius: 4px;
  border: 1px solid rgba(255, 255, 255, 1);
  ${flexCenterCss};
`;

// 标签样式
export const StyledH3 = styled.h3`
  height: 26px;
  width: 100%;
  line-height: 26px;
  font-size: 17px;
  letter-spacing: 1.1px;
  color: rgba(3, 3, 3, 1);
  ${ellipsisCss};
`;

export const StyledP = styled.p`
  height: 24px;
  width: 100%;
  line-height: 24px;
  font-size: 14px;
  color: ${(props) => props.theme.colorFont2};
  ${ellipsisCss};
`;

export const StyledImg = styled.img<{ width?: number; isRound?: boolean }>`
  width: ${({ width }) => width || 90}px;
  height: ${({ width }) => width || 90}px;
  margin-right: 10px;
  border-radius: ${({ width, isRound }) => (isRound ? (width || 90) / 2 : 4)}px;
`;

// 功能型样式
export const StyledFlexColumn = styled.div`
  width: calc(100vw - 170px);
  padding-bottom: 4px;
  ${flexColumnCss};
`;

export const StyledFlexRow = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
`;

export const StyledFlex = styled.div`
  display: flex;
  flex-direction: column;
  align-items: baseline;
  justify-content: center;
  height: 80px;
  ${ellipsisCss};
`;

export const StyledRowScroll = styled.div`
  width: 100vw;
  overflow: scroll;
  > div {
    display: flex;
  }
`;

export const StyledBlur = styled.div`
  filter: blur(2px);
`;

export const StyledTime = styled.time`
  font-size: 12px;
  color: ${(props) => props.theme.colorFont2};
`;
