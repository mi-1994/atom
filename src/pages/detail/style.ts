import styled from 'styled-components';
import bg from './img/bg.png';

export const StyleDetail = styled.div`
  width: 200px;
  height: 150px;
  background: url(${bg});
`;
