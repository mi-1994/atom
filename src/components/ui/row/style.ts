/** Copyright © 2013-2021 DataYes, All Rights Reserved. */

import styled from 'styled-components';

export const StyledTitle = styled.div`
  > span {
    font-size: 30px;
    font-weight: bold;
    color: ${(props) => props.theme.colorTheme};

    &:nth-child(2) {
      font-size: 20px;
      font-weight: normal;
    }
    &:nth-child(3) {
      margin: 0 2px;
      font-size: 20px;
      color: ${(props) => props.theme.colorFont1};
    }
  }
`;

export const StyledSubTitle = styled.div`
  display: flex;
  align-items: center;
  height: 24px;
  margin: 30px 0;
  font-weight: bold;
  font-size: 20px;
  color: ${(props) => props.theme.colorFont1};
  > p {
    height: 18px;
    width: 4px;
    margin-right: 5px;
    background-color: ${(props) => props.theme.colorTheme};
  }
`;

export const StyledText = styled.div`
  display: flex;
  > div {
    &:nth-child(1) {
      height: 12px;
      width: 16px;
      margin-top: 5px;
      border-radius: 6px;
      margin-right: 5px;
      color: ${(props) => props.theme.colorFont3};
      background-color: ${(props) => props.theme.colorTheme};
      font-weight: bold;
    }
    &:nth-child(2) {
      line-height: 30px;
      color: ${(props) => props.theme.colorFont2};
    }
  }
`;
