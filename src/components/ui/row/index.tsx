/** Copyright © 2013-2021 DataYes, All Rights Reserved. */

import React from 'react';
import { StyledTitle, StyledSubTitle, StyledText } from './style';

export const RowNumTitle = () => (
  <StyledTitle>
    <span>9</span>
    <span>/</span>
    <span>引用组件</span>
    <span>.</span>
  </StyledTitle>
);

export const RowBorderTitle = () => (
  <StyledSubTitle>
    <p />
    <span>什么是引用组件？</span>
  </StyledSubTitle>
);

export const RowRoundText = () => (
  <StyledText>
    <div />
    <div>中标题，在右侧面板中修改文本颜色，中标题，在右侧面板中修改文本颜色。</div>
  </StyledText>
);
