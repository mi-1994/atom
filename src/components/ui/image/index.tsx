import React from 'react';
import { ImageProps, StyleImage } from './style';
import defaultJpg from './img/default.jpg';

export const Image = ({ src, ...rest }: ImageProps) => (
  <StyleImage {...rest}>
    <img src={src || defaultJpg} alt="" />
  </StyleImage>
);
