import styled from 'styled-components';

export interface ImageStyleProps {
  width: number | string;
  height?: number | string;
  radius?: number;
  right?: number;
}

export interface ImageProps extends ImageStyleProps {
  src?: string;
}

export const StyleImage = styled.div<ImageStyleProps>`
  > img {
    width: ${({ width }) => width}px;
    height: ${({ height, width }) => height || width}px;
    margin-right: ${({ right }) => right}px;
    border-radius: ${({ radius }) => radius || 4}px;
  }
`;
