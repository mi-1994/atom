/** Copyright © 2013-2021 DataYes, All Rights Reserved. */

import styled from 'styled-components';
import { flexCenterCss } from 'styles/common';

export const StyledTagText = styled.span`
  color: ${(props) => props.theme.colorTheme};
`;

export const StyledTagBtn = styled.div`
  height: 17px;
  width: 50px;
  background-color: ${(props) => props.theme.colorTheme};
  color: ${(props) => props.theme.colorFont3};
  border-radius: 10px;
  font-size: 12px;
  ${flexCenterCss};
`;
