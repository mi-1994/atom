/** Copyright © 2013-2021 DataYes, All Rights Reserved. */

import React from 'react';
import { StyledTagText, StyledTagBtn } from './style';

interface Props {
  text: string;
}

export const TagText = ({ text }: Props) => <StyledTagText>{`${[text]}`}</StyledTagText>;

export const TagBtn = ({ text }: Props) => <StyledTagBtn>{text}</StyledTagBtn>;
