import React, { ReactChild } from 'react';
import { Link } from 'react-router-dom';
import { StyleButton } from './style';

export const Button = ({ children }: { children: ReactChild }) => (
  <StyleButton>{children}</StyleButton>
);

export const LinkButton = ({ children, path }: { children: ReactChild; path: string }) => (
  <Link to={path}>
    <Button>{children}</Button>
  </Link>
);

export { Link };
