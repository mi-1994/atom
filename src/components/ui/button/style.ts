import styled from 'styled-components';
import { flexCenterCss } from 'styles/common';

export const StyleButton = styled.div`
  margin-top: 30px;
  height: 55px;
  width: 100%;
  border-radius: 8px;
  background-color: ${(props) => props.theme.colorTheme};
  color: #fff;
  box-shadow: 0 4px 10px 0 rgba(232, 86, 86, 0.6);
  letter-spacing: 2px;
  font-size: 16px;
  ${flexCenterCss};
`;
