/** Copyright © 2013-2021 DataYes, All Rights Reserved. */

import React from 'react';
import styled from 'styled-components';

export const StyledColors = styled.div`
  display: flex;
  padding: 0 20px;
  > p {
    height: 10px;
    width: 10px;
    border-radius: 5px;
    margin-right: 10px;
  }
`;

export const Colors = () => (
  <StyledColors>
    <p />
  </StyledColors>
);
