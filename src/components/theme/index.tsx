import React, { ReactChild } from 'react';
import { ThemeProvider } from 'styled-components';
import { GlobalStyle } from 'styles/global';
import useHotReload from 'hooks/hot-reload';
import { theme } from 'styles/theme';

export default function GlobalThemeStyle({ children }: { children: ReactChild }) {
  useHotReload();

  return (
    <ThemeProvider theme={theme}>
      <GlobalStyle />
      {children}
    </ThemeProvider>
  );
}
