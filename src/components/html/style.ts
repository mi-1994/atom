import styled from 'styled-components';

export const StyleHtml = styled.div`
  p {
    margin-top: 10px;
    line-height: 20px;
    font-size: 14px;
  }
  img {
    width: 100%;
  }
`;
