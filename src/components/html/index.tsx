import React from 'react';
import { StyleHtml } from './style';

export const Html = ({ html }: { html: string }) => (
  <StyleHtml dangerouslySetInnerHTML={{ __html: html }} />
);
