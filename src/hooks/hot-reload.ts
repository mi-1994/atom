import { useEffect } from 'react';

/**
 * 基于WebSocket的热重载
 * 结合hot-reload.js使用
 */
export default function useHotReload() {
  useEffect(() => {
    // 只有在开发本地环境 才开启
    if (window.location.hostname === 'localhost') {
      const ws = new WebSocket('ws://localhost:4000');

      ws.onmessage = ({ data }) => {
        // 监听到客户端打包文件变化
        if (data === 'reloading') {
          console.log('开始热重载🔥...');
        }
        // 服务端重启成功
        if (data === 'reload') {
          window.location.reload();
        }
      };
    }
  }, []);
}
