import { useEffect, useState } from 'react';

export const useAnimate = (animateClass = 'animate__animated animate__pulse animate__infinite') => {
  const [className, setClassName] = useState('');

  useEffect(() => {
    setTimeout(() => {
      setClassName(animateClass);
    }, 2000);
  }, []);

  return className;
};
