import { createGlobalStyle } from 'styled-components';
import { ellipsisCss } from 'styles/common';

export const GlobalStyle = createGlobalStyle`
   html {
    text-size-adjust: 100%;
     body {
        font-size: 14px;
        background: #fff;
        min-height: 100vh;
        a {
          text-decoration:none;
        }
      }
    }
  * {
    margin: 0;
    -webkit-tap-highlight-color: transparent;
    box-sizing: border-box;
  }
  h3 {
    height: 26px;
    width: 100%;
    line-height: 26px;
    font-size: 17px;
    letter-spacing: 1.1px;
    color: rgba(3, 3, 3, 1);
    ${ellipsisCss};
  }
  time {
    font-size: 12px;
  }
`;
