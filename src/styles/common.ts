/** Copyright © 2013-2021 DataYes, All Rights Reserved. */

import styled, { css } from 'styled-components';

export const flexCenterCss = css`
  display: flex;
  align-items: center;
  justify-content: center;
`;

export const flexBetCss = css`
  display: flex;
  align-items: center;
  justify-content: space-between;
`;

export const flexColumnCss = css`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
`;

export const ellipsisCss = css`
  overflow: hidden;
  white-space: nowrap;
  text-overflow: ellipsis;
`;

export const StyledLineEllipsis = styled.div<{ line?: number }>`
  display: -webkit-box;
  text-overflow: ellipsis;
  overflow: hidden;
  -webkit-box-orient: block-axis;
  -webkit-line-clamp: ${({ line }) => line || 1};
`;

export const lineEllipsisCss = (line?: number) => css`
  display: -webkit-box;
  text-overflow: ellipsis;
  overflow: hidden;
  -webkit-box-orient: block-axis;
  -webkit-line-clamp: ${line || 1};
`;

export const triangleCSS = (w: number, h: number, direction = 'up', color = '#a5a5a5') => {
  const isTop = direction === 'up';

  return css`
    display: inline-block;
    height: 0;
    width: 0;
    border-right: ${w / 2}px solid transparent;
    border-left: ${w / 2}px solid transparent;
    border-top: ${h}px solid ${isTop ? 'transparent' : color};
    border-bottom: ${h}px solid ${!isTop ? 'transparent' : color};
    transform: translateY(${isTop ? h / -2 : h / 2}px);
  `;
};

// 0.5像素外边框
export const halfPixelBorder = css`
  &::before {
    content: '';
    width: 200%;
    height: 200%;
    border: 1px solid #e5e5e5;
    border-radius: 8px;
    position: absolute;
    left: 0;
    top: 0;
    transform: scale(0.5) translateX(-50%) translateY(-50%);
    z-index: 10;
    box-sizing: border-box;
  }
`;

// 单根横向分割线
export const halfPixelLine = css`
  &:after {
    content: ' ';
    height: 1px;
    width: 100%;
    background-color: #e5e5e5;
    position: absolute;
    left: 0;
    bottom: 0;
    transform: scaleY(0.5);
    z-index: 10;
  }
`;

// 单根竖向分割线
export const halfPixelSeparator = css`
  &:before {
    content: '';
    height: 100%;
    width: 1px;
    background-color: #e5e5e5;
    position: absolute;
    top: 0;
    right: 0;
    transform: scaleX(0.5) translateX(100%);
    z-index: 10;
  }
`;

export const cardCss = css`
  position: relative;
  padding: 15px;
  margin-top: 20px;
  background-color: ${(props) => props.theme.colorBg2};
  box-shadow: 0px 4px 8px ${(props) => props.theme.colorShadow1};
  border-radius: 6px;
`;
