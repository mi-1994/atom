export const theme = {
  colorBg1: 'rgba(240, 242, 245, 1)', // 一级背景色(浅灰色)
  colorBg2: 'rgba(255, 255, 255, 1)', // 二级背景色(白色)
  colorTheme: 'rgba(224, 88, 88, 1)', // 主题色(棕红色)
  colorFont1: 'rgba(3, 3, 3, 1)', // 字号色(黑色)
  colorFont2: 'rgba(188, 188, 188, 1)', // 字号色(灰色)
  colorFont3: 'rgba(255, 255, 255, 1)', // 字号色(白色)
  colorShadow1: 'rgba(0, 0, 0, 0.15)', // 阴影色(透明黑)
};
