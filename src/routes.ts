import Home from 'pages/home';
import ColumnList from 'pages/column/list';
import ColumnDetail from 'pages/column/detail';
import ArticleDetail from 'pages/article/detail';
import Login from './pages/login';
import List from './pages/list';
import Detail from './pages/detail';
import Demo from './pages/demo';
import Zwl from './pages/zwl';

export default [
  {
    path: '/',
    Component: Home,
  },
  {
    path: '/home',
    Component: Home,
  },
  {
    path: '/login',
    Component: Login,
  },
  {
    path: '/list',
    Component: List,
  },
  {
    path: '/detail',
    Component: Detail,
  },
  {
    path: '/column/list',
    Component: ColumnList,
  },
  {
    path: '/column/detail/:id',
    page: '/column/detail',
    Component: ColumnDetail,
  },
  {
    path: '/article/detail/:id',
    page: '/article/detail',
    Component: ArticleDetail,
  },
  {
    path: '/demo',
    Component: Demo,
  },
  {
    path: '/zwl',
    Component: Zwl,
  },
];
