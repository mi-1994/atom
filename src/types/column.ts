export interface Column {
  name: string;
  desc: string;
  content: string;
  src: string[];
  isOpen: boolean;
}
