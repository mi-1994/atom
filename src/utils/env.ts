/**
 * 进程运行 环境
 * @returns 是否是开发环境
 */
export const isDev = () => process.env.NODE_ENV !== 'production';

/**
 *
 * @returns
 */
// export const isClientSide = () => process && process.browser;
