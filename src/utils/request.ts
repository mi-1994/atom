import axios from 'axios';

// const localApi = 'https://localhost:8082/';

const axiosInstance = axios.create({
  baseURL: 'https://mi-atom.com:8082/',
  timeout: 1000,
  headers: {
    'Content-Type': 'application/json',
    'Accept-Language': 'zh-cn',
  },
});

export const get = (url: string) => axiosInstance.get(url).then(({ data }) => data.data);
export const post = (url: string) => axiosInstance.post(url).then(({ data }) => data.data);
export const put = (url: string) => axiosInstance.put(url).then(({ data }) => data.data);
