import { useNavigate } from 'react-router-dom';

export function useNav() {
  const push = useNavigate();

  return push;
}
