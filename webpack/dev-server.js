const nodeExternals = require('webpack-node-externals');
const { merge } = require('webpack-merge');
const { resolve } = require('path');
const getConfig = require('./base.js');

module.exports = merge(getConfig(true), {
  mode: 'development',
  entry: resolve(__dirname, '/src/server/middleware/index.ts'), // 入口文件
  target: 'node', // node端运行
  externals: [nodeExternals()], // node外部环境 来排除不需要打包的模块
  output: {
    filename: 'app.js',
    libraryTarget: 'commonjs2', // 设置为common规范 module.export
    path: resolve(__dirname, '../build/server'),
    clean: true, // 每次构建清除上次的包
  },
});
