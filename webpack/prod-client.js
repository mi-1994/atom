const { merge } = require('webpack-merge');
const { resolve } = require('path');
const getConfig = require('./base.js');

module.exports = merge(getConfig(), {
  mode: 'production',
  entry: resolve(__dirname, '/src/pages/_app.tsx'),
  output: {
    filename: 'app.js',
    path: resolve(__dirname, '../build/client/'),
    clean: true, // 每次构建清除上次的包
  },
});
