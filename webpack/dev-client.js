const { merge } = require('webpack-merge');
const { resolve } = require('path');
const getConfig = require('./base.js');

module.exports = merge(getConfig(), {
  mode: 'development',
  // devtool: 'eval-source-map', // 源代码与打包后代码映射 调试模式
  output: {
    filename: 'app.js', // 通过script标签 运行在游览器
    path: resolve(__dirname, '../build/client'),
    clean: true, // 每次构建清除上次的包
  },
});
