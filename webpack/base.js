// const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const TerserPlugin = require('terser-webpack-plugin');
// const SentryCliPlugin = require('@sentry/webpack-plugin');
const { resolve } = require('path');

module.exports = (isServer) => ({
  entry: resolve(__dirname, '/src/pages/_app.tsx'), // 入口文件
  devtool: 'source-map',
  // 用来设置引用模块
  resolve: {
    // 设置可以引用的文件的扩展名 引用模块包括node_modules里的文件，有js ts等
    extensions: ['.js', '.ts', '.tsx'],
    // 告诉 webpack 解析模块时应该搜索的目录，不打包只引用
    modules: ['node_modules'],
  },
  module: {
    rules: [
      {
        test: /\.ts|\.tsx$/,
        exclude: /node_modules/,
        use: [
          {
            loader: 'babel-loader', // 关联babel.config.js
          },
        ],
      },
      // 加在图片资源 webpack 不再使用 url-loader html-loader file-loader
      {
        test: /\.png|\.jpg$/,
        type: 'asset/resource',
        generator: {
          publicPath: '/static/', // 引用路径
          filename: '../static/images/[hash][ext][query]', // 打包路径 ..和client同级
          emit: isServer, // server 端不用导出图片
        },
      },
    ],
  },
  plugins: [
    // 清理打包文件，缓解内存压力
    // new CleanWebpackPlugin({
    //   allowcleanpatternsoutsideproject: true, // 允许删除当前工作目录之外的文件
    // }),
    // new SentryCliPlugin({
    //   // urlPrefix: 'https://mi-atom.com/static/chunks',
    //   org: 'binance-us', // 组织名称
    //   project: 'us-wallet-ui', // 项目名称
    //   release: 'atom-release-1',
    //   include: './build/client', // 将打包js map 上传到sentry
    //   authToken: '562c2b47b0b947a09b97d854f642f72032a75d204bf542c19515944d113831a8',
    // }),
  ],
  optimization: {
    minimizer: [
      new TerserPlugin({
        parallel: 4,
        terserOptions: {
          parse: {
            ecma: 8,
          },
          compress: {
            ecma: 5,
            warnings: false,
            comparisons: false,
            inline: 2,
          },
          mangle: {
            safari10: true,
          },
          output: {
            ecma: 5,
            comments: false,
            ascii_only: true,
          },
        },
      }),
    ],
  },
});
